//
//  SensoresTableViewCell.swift
//  airelibre
//
//  Created by Christian Colman on 2022-11-10.
//

import UIKit

class SensoresTableViewCell: UITableViewCell {


    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var descripcionName: UILabel!
    @IBOutlet weak var metros: UILabel!
    @IBOutlet weak var icono: UIImageView!
    @IBOutlet weak var valor: UILabel!
    @IBOutlet weak var emoji: UILabel!
    
    @IBOutlet weak var stackVIewNeedCorner: UIStackView!
    @IBOutlet weak var compartir: UIButton!
    
    @IBOutlet weak var verMas: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewWithTag(1)?.backgroundColor = UIColor.white
        
        // Initialization code
//        icono.image =
        stack.backgroundColor = UIColor.green
        stack.layer.cornerRadius = 10
            // border
        stack.layer.borderWidth = 1.0
        stack.layer.borderColor = UIColor.black.cgColor
            // shadow
        stack.layer.shadowColor = UIColor.black.cgColor
        stack.layer.shadowOffset = CGSize(width: 3, height: 3)
        stack.layer.shadowOpacity = 0.7
        stack.layer.shadowRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        stackVIewNeedCorner.cornerRadius = 20
        
    }
    
}

