//
//  SheetViewController.swift
//  sheetViewController
//
//  Created by Christian Colman on 2022-11-08.
//

import UIKit
import GoogleMaps

class SheetViewController: UIViewController, UISheetPresentationControllerDelegate {
    private var homeViewModel:HomeViewModel!
    private var sensores : [SensorResponse] = []
    private var arrayDistancia : [Float] = []
//    private var sensorList:
    
    @IBOutlet weak var sensorList: UITableView!
    override var sheetPresentationController: UISheetPresentationController {
        presentationController as! UISheetPresentationController
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        sensorList.backgroundColor = UIColor.systemGreen
        self.sensorList.layer.cornerRadius = 35
//        se
        makeButton()
        callService()
//        MarcadoresList(list: [SensorResponse])
        // Table View
        sensorList.delegate = self
        sensorList.dataSource = self
//        sensorList.frame = sensorList.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        sensorList.layer.cornerRadius = 20
        sensorList.layer.masksToBounds = true
        
        
      
        
        
        // Regsitrar la nueva celda
        sensorList.register(UINib(nibName: "SensoresTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")
        
        
        
    // ###########################
        
        
//        print (SensorResponse)
        sheetPresentationController.delegate = self
        sheetPresentationController.selectedDetentIdentifier = .medium
        sheetPresentationController.prefersGrabberVisible = true
        sheetPresentationController.detents = [
            .medium(),
//            .large()
        ]

    }
    func makeButton() {
        let button = UIButton(configuration: .filled(),primaryAction: .init(handler: {_ in
            
            self.sheetPresentationController.dismissalTransitionDidEnd(true)


            
        }))
        button.setTitle("X", for: .normal)
        button.configuration?.cornerStyle = .capsule
        view.addSubview(button)
        
        
        //Setup constarints
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
    
    
    
    
    private func callService(){
        self.homeViewModel = HomeViewModel()
        self.homeViewModel.bindHomeViewModelToController = {
            
            if(self.homeViewModel.sensor.count <= 0){
//                self.btnCallService.isHidden = false
            }else{
                self.MarcadoresList(list: self.homeViewModel.sensor)
//                self.btnCallService.isHidden = true
            }
        }
    
//        DispatchQueue.main.async {
//            self.sensores.reloadData()
//        }
    }
    
    private func MarcadoresList(list:[SensorResponse]){
        DispatchQueue.main.async {
                
            for sensor in list{
                self.sensores.append(sensor)
            }
            self.sensorList.reloadData()
            
        }
        print ("holaaaaaa este es sensores \n\n\n", self.homeViewModel.sensor!)
        
//        for idx in sensores {
//            let distanceMetro = GoogleMaps.
//        }
    }
}



extension SheetViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sensores.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print ("Count de sensores es ", sensores.count)
//        DispatchQueue.main.async {
//            self.sensorList.reloadData()
//        }
//
            
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
//        DispatchQueue.main.async {
//            self.sensorList.reloadData()
//        }
        let celda = sensorList.dequeueReusableCell(withIdentifier: "celda", for: indexPath) //as! SensoresTableViewCell
        
        //        celda.sensorName.text = sensores[indexPath.row].sensor
//        celda.layer.masksToBounds = false
//        celda.layer.cornerRadius = 20
//        celda.stackVIewNeedCorner.layer.cornerRadius = 100
       // celda.descripcionName.text = sensores[indexPath.row].description
        celda.textLabel?.text = "pass"
//        celda.descripcionName.text = sensores[indexPath.row].description
//        celda.descripcion.text = sensores[indexPath.row].description
        return celda
    }
    
    
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 100
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 100
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return 100
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
//    override func viewDidLayoutSubviews() {
//
//    }
}
